- Week2-miniProj-zg105

In this project, I implemented a simple AWS Lambda function that sums up all the data in given json file, and return the sum result. 

To start with, run "make watch" to monitor port 9000：
![](gzc2.png)

We can then run "make invoke" to invoke the lambda function using given json file:
![](gzc1.png)

Here is the result:
![](gzc3.png)
