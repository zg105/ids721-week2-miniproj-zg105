use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde_json::Value;
use serde::Serialize;

// #[derive(Deserialize)]
// struct Request {
//     // greeting: String,
//     // number1: i32,
//     // number2: i32,
// }

#[derive(Serialize)]
struct Response {
    req_id: String,
    //msg: String,
    sum: i32,
}

//A marco polo function that return polo if you pass in Marco
// fn marco_polo_greeting(greeting: &str) -> String {
//     if greeting == "Marco" {
//         "Polo".to_string()
//     } else {
//         "No".to_string()
//     }
// }
fn sum_numbers(value: &Value) -> i32 {
    match value {
        Value::Number(num) => num.as_i64().unwrap_or(0) as i32,
        Value::Array(arr) => arr.iter().map(|v| sum_numbers(v)).sum(),
        Value::Object(obj) => obj.values().map(|v| sum_numbers(v)).sum(),
        _ => 0,
    }
}
async fn function_handler(event: LambdaEvent<Value>) -> Result<Response, Error> {

    let sum = sum_numbers(&event.payload);

    let resp = Response {
        req_id: event.context.request_id,
        sum,
    };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
